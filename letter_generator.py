#first you need to install report lab by pip install reportlab



from tkinter import *
from reportlab.pdfgen import canvas 
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib import colors
from urllib.request import urlopen
from PIL import Image


def reader():
        if letter.get()==1:
            sk=[]
            def get_data():
                sk.append(ent_certificate.get())
                ent_certificate.delete(0,END)


            def reader_resume():
                
                fileName = ent_filename.get()+".pdf"
                documentTitle="Resume"
                title = 'Resume'
                subtitle=[
                'To work with an organization which provides me the opportunity to improve my skills' , 
                'and knowledge to grow along with the organization. Positive & optimistic '+ent_branch.get(),
                "always willing to innovate the new things which can improve the existing ",
                "                                                                things." 
                ]
                for_skill=ent_branch.get()+ ' Skills & Certification:-'

                personal_details=[
                'Date Of Birth:',
                '',
                'Languages Known:',
                '',
                'Hobbies: ',
                '',
                'Address: '    
                ]

            
                pdf = canvas.Canvas(fileName)  #name of our file
                pdf.setTitle(documentTitle)    #title to be shown on chrome




                #for font of title

                
                pdf.setFont('Times-Bold',30)
                pdf.drawCentredString(300, 770, title)
                ##########################################



                #for large amount of text


                
                text = pdf.beginText(100,720)
                text.setFont("Times-Roman", 12)
                text.setFillColor(colors.black)
                for line in subtitle:
                    text.textLine(line)
                pdf.drawText(text)

                email_id = pdf.beginText(190,650)
                email_id.setFont("Times-Bold", 12)
                email_id.setFillColor(colors.blue)
                email_id.textLine(ent_email.get())
                pdf.drawText(email_id)


                phone_no = pdf.beginText(378,650)
                phone_no.setFont("Times-Bold", 12)
                phone_no.setFillColor(colors.black)
                phone_no.textLine(ent_phone.get())
                pdf.drawText(phone_no)

                nav=pdf.beginText(205,610)
                nav.setFont("Times-Bold", 20)
                nav.setFillColor(colors.black)
                nav.textLine(ent_name.get())
                pdf.drawText(nav)

                ed=pdf.beginText(35,570)
                ed.setFont("Times-Bold", 15)
                ed.setFillColor(colors.chocolate)
                ed.textLine(text="Education & Credentials:-")
                pdf.drawText(ed)
                pdf.line(40, 565, 190, 565)



                skill=pdf.beginText(35,400)
                skill.setFont("Times-Bold", 15)
                skill.setFillColor(colors.chocolate)
                skill.textLine(for_skill)
                pdf.drawText(skill)
                pdf.line(35, 395, 205, 395)

                cer=pdf.beginText(95,380)
                cer.setFont("Times-Roman", 12)
                cer.setFillColor(colors.black)
                for i in sk:
                    cer.textLine(i)
                pdf.drawText(cer)

                pd=pdf.beginText(35,250)
                pd.setFont("Times-Bold", 15)
                pd.setFillColor(colors.chocolate)
                pd.textLine(text="Personal Details:-")
                pdf.drawText(pd)
                pdf.line(35, 245, 145, 245)


                p_d=pdf.beginText(95,230)
                p_d.setFont("Times-Roman", 12)
                p_d.setFillColor(colors.black)
                for lines in personal_details:
                    p_d.textLine(lines)
                pdf.drawText(p_d)    


                db=pdf.beginText(170,230)
                db.setFont("Times-Roman", 12)
                db.setFillColor(colors.black)
                db.textLine(ent_dob.get())
                pdf.drawText(db)


                ln=pdf.beginText(195,201)
                ln.setFont("Times-Roman", 12)
                ln.setFillColor(colors.black)
                ln.textLine(ent_lang.get())
                pdf.drawText(ln)


                hb=pdf.beginText(140,172)
                hb.setFont("Times-Roman", 12)
                hb.setFillColor(colors.black)
                hb.textLine(ent_hobbies.get())
                pdf.drawText(hb)


                ad=pdf.beginText(140,143)
                ad.setFont("Times-Roman", 12)
                ad.setFillColor(colors.black)
                ad.textLine(ent_address.get())
                pdf.drawText(ad)


                pdf.line(150, 555, 150,440 ) #v1
                pdf.line(182, 555, 182,440 )  #v2
                pdf.line(270,555,270,440)    #v3
                pdf.line(365,555,365,440)     #v4
                pdf.line(465,555,465,440)      #v5


                pdf.line(150, 555, 465,555 )  #h1
                pdf.line(150,440,465,440)     #h2

                pdf.line(150,537,465,537)  #gh1
                pdf.line(150,465,465,465)  #ghlast
                pdf.line(150,490,465,490)  #ghlast2



                yr=pdf.beginText(154,543)
                yr.setFont("Times-Bold", 12)
                yr.setFillColor(colors.black)
                yr.textLine(text='year')
                pdf.drawText(yr)


                ql=pdf.beginText(192,543)
                ql.setFont("Times-Bold", 12)
                ql.setFillColor(colors.black)
                ql.textLine(text='Qualification')
                pdf.drawText(ql)


                fr=pdf.beginText(300,543)
                fr.setFont("Times-Bold", 12)
                fr.setFillColor(colors.black)
                fr.textLine(text='From')
                pdf.drawText(fr)

                cl=pdf.beginText(398,543)
                cl.setFont("Times-Bold", 12)
                cl.setFillColor(colors.black)
                cl.textLine(text='Class')
                pdf.drawText(cl)

                ss=pdf.beginText(205,450)
                ss.setFont("Times-Roman", 12)
                ss.setFillColor(colors.black)
                ss.textLine(text='SSC')
                pdf.drawText(ss)

                hs=pdf.beginText(205,475)
                hs.setFont("Times-Roman", 12)
                hs.setFillColor(colors.black)
                hs.textLine(text='HSC')
                pdf.drawText(hs)



                grt=pdf.beginText(188,512)
                grt.setFont("Times-Roman", 12)
                grt.setFillColor(colors.black)
                grt.textLine(ent_gr.get())
                pdf.drawText(grt)



                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(165, 450, ent_ssc.get())

                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(165, 475, ent_hsc.get())

                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(165, 512, ent_be.get())

                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(315, 450, ent_from_ssc.get())

                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(315, 475, ent_from_hsc.get())

                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(315, 512, ent_from_gr.get())


                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(410, 450, ent_marks_ssc.get())

                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(410, 475, ent_marks_hsc.get())

                pdf.setFont('Times-Roman',12)
                pdf.drawCentredString(410, 512, ent_marks_gr.get())



                
                
                url = "https://png.pngitem.com/pimgs/s/26-260217_gmail-logo-png-gmail-png-transparent-png.png"
                img_mail = Image.open(urlopen(url))
                pdf.drawInlineImage(img_mail, 175, 648,width=13,height=13)


                url = "https://png.pngitem.com/pimgs/s/20-202269_phone-number-telephone-svg-png-icon-free-download.png"
                img_phone = Image.open(urlopen(url))
                pdf.drawInlineImage(img_phone, 364, 648,width=13,height=10)


                pdf.setStrokeColor(colors.green)
                pdf.line(250, 765, 355, 765)

                pdf.save()
                
                ent_filename.delete(0,END)
                ent_name.delete(0,END)
                ent_branch.delete(0,END)
                ent_email.delete(0,END)
                ent_phone.delete(0,END)
                ent_dob.delete(0,END)
                ent_lang.delete(0,END)
                ent_hobbies.delete(0,END)
                ent_address.delete(0,END)
                ent_be.delete(0,END)
                ent_hsc.delete(0,END)
                ent_ssc.delete(0,END)
                ent_gr.delete(0,END)
                ent_from_gr.delete(0,END)
                ent_from_hsc.delete(0,END)
                ent_from_ssc.delete(0,END)
                ent_marks_gr.delete(0,END)
                ent_marks_hsc.delete(0,END)
                ent_marks_ssc.delete(0,END)
                
                
                    





            root=Tk( )
            root.title("Resume frame")#name of window
            f=Frame(root)


            lbl_filename=Label(root,text="Name to be given to file:")
            lbl_filename.place(x=10,y=10)
            ent_filename=Entry(root,bd=4)
            ent_filename.place(x=150,y=10)
            
            
            lbl_name=Label(root,text="Your name")
            lbl_name.place(x=10,y=50)
            ent_name=Entry(root,bd=4)
            ent_name.place(x=150,y=50)

            lbl_branch=Label(root,text="Your branch name")
            lbl_branch.place(x=10,y=90)
            ent_branch=Entry(root,bd=4)
            ent_branch.place(x=150,y=90)

            lbl_email=Label(root,text="Your email-id ")
            lbl_email.place(x=10,y=130)
            ent_email=Entry(root,bd=4)
            ent_email.place(x=150,y=130)

            lbl_phone=Label(root,text="Your phone number ")
            lbl_phone.place(x=10,y=170)
            ent_phone=Entry(root,bd=4)
            ent_phone.place(x=150,y=170)

            lbl_dob=Label(root,text="Your Date of birth ")
            lbl_dob.place(x=10,y=210)
            ent_dob=Entry(root,bd=4)
            ent_dob.place(x=150,y=210)

            lbl_lang=Label(root,text="Your known languages")
            lbl_lang.place(x=10,y=250)
            ent_lang=Entry(root,bd=4)
            ent_lang.place(x=150,y=250)

            lbl_hobbies=Label(root,text="Your hobbies")
            lbl_hobbies.place(x=10,y=290)
            ent_hobbies=Entry(root,bd=4)
            ent_hobbies.place(x=150,y=290)

            lbl_address=Label(root,text="Your address")
            lbl_address.place(x=10,y=330)
            ent_address=Entry(root,bd=4)
            ent_address.place(x=150,y=330)

            lbl_certificate=Label(root,text="Enter your skils/certificates ")
            lbl_certificate.place(x=10,y=370)
            ent_certificate=Entry(root,bd=4)
            ent_certificate.place(x=150,y=370)



            lbl_ssc=Label(root,text="Year of qualifiaction of ss")
            lbl_ssc.place(x=450,y=10)
            ent_ssc=Entry(root,bd=4)
            ent_ssc.place(x=610,y=10)

            lbl_hsc=Label(root,text="Year of qualifiaction of hss")
            lbl_hsc.place(x=450,y=50)
            ent_hsc=Entry(root,bd=4)
            ent_hsc.place(x=610,y=50)

            lbl_gr=Label(root,text="In what field you are/had \ndone your Graduation")
            lbl_gr.place(x=450,y=90)
            ent_gr=Entry(root,bd=4)
            ent_gr.place(x=610,y=90)

            lbl_be=Label(root,text="Applied year for Graduation")
            lbl_be.place(x=450,y=130)
            ent_be=Entry(root,bd=4)
            ent_be.place(x=610,y=130)

            lbl_from_ssc=Label(root,text="From which board you have \ndone your ssc")
            lbl_from_ssc.place(x=450,y=170)
            ent_from_ssc=Entry(root,bd=4)
            ent_from_ssc.place(x=610,y=170)

            lbl_from_hsc=Label(root,text="From which board you have \n done your hsc")
            lbl_from_hsc.place(x=450,y=210)
            ent_from_hsc=Entry(root,bd=4)
            ent_from_hsc.place(x=610,y=210)

            lbl_from_gr=Label(root,text="From which college you have \n done your qualification")
            lbl_from_gr.place(x=450,y=250)
            ent_from_gr=Entry(root,bd=4)
            ent_from_gr.place(x=610,y=250)

            lbl_marks_ssc=Label(root,text="Result of ssc")
            lbl_marks_ssc.place(x=450,y=290)
            ent_marks_ssc=Entry(root,bd=4)
            ent_marks_ssc.place(x=610,y=290)

            lbl_marks_hsc=Label(root,text="Result of hsc")
            lbl_marks_hsc.place(x=450,y=330)
            ent_marks_hsc=Entry(root,bd=4)
            ent_marks_hsc.place(x=610,y=330)

            lbl_marks_gr=Label(root,text="Result of graduation")
            lbl_marks_gr.place(x=450,y=370)
            ent_marks_gr=Entry(root,bd=4)
            ent_marks_gr.place(x=610,y=370)




            btn=Button(root,text="ADD MORE",command=get_data)
            btn.place(x=285,y=370)

            btn=Button(root,text="download",command=reader_resume)
            btn.place(x=335,y=425)
            lbl_result=Label(root,text="click download")
            lbl_result.place(x=325,y=450)

            root.geometry('800x500+350+150')#width height x and y position to display
            root.mainloop( )
        elif letter.get()==2:
            def reader_recommendation():
                documentTitle="Recommendation letter"
                title = 'Recommendation letter'
                fileName = ent_filename.get()+".pdf"

                if ent_Gender.get() == 'male':
                    main_part=[
                        'My name is ' +ent_your_name.get()+ ' and I am a ' +ent_job_position.get()+ ' with ' +ent_company_name.get(),
                        ' I highly recommend ' +ent_applicant_name.get()+ ' who is my agent',
                        'for ' +ent_working_year.get()+ ' years.',
                        '',
                        'I can assure you that ' +ent_applicant_name.get()+ ' has good work ethics and',
                        'exemplary skills. ' +ent_applicant_name.get()+ ' is a constant top performer in our',
                        'department. ' +ent_applicant_name.get()+ ' has always exceeded scorecard metrics',
                        'and received numerous awards.',
                        '',
                        ent_applicant_name.get()+ ' is a team player and proactively help his',
                        'teammates regarding hard technical issues.',
                        '',
                        'I strongly recommend ' +ent_applicant_name.get()+ ' as an excellent and professional',
                        'worker. I am willing to provide more information if needed.',
                        '',
                        '',
                        'Warm regards,'
                        ]
                elif ent_Gender.get() == 'female':
                    main_part=[
                        'My name is ' +ent_your_name.get()+ ' and I am a ' +ent_job_position.get()+ ' with ' +ent_company_name.get(),
                        ' I highly recommend ' +ent_applicant_name.get()+ ' who is my agent',
                        'for ' +ent_working_year.get()+ ' years.',
                        '',
                        'I can assure you that ' +ent_applicant_name.get()+ ' has good work ethics and',
                        'exemplary skills. ' +ent_applicant_name.get()+ ' is a constant top performer in our',
                        'department. ' +ent_applicant_name.get()+ ' has always exceeded scorecard metrics',
                        'and received numerous awards.',
                        '',
                        ent_applicant_name.get()+ ' is a team player and proactively help her',
                        'teammates regarding hard technical issues.',
                        '',
                        'I strongly recommend ' +ent_applicant_name.get()+ ' as an excellent and professional',
                        'worker. I am willing to provide more information if needed.',
                        '',
                        '',
                        'Warm regards,'
                        ]    
                else:
                    main_part=[
                        'My name is ' +ent_your_name.get()+ ' and I am a ' +ent_job_position.get()+ ' with ' +ent_company_name.get(),
                        ' I highly recommend ' +ent_applicant_name.get()+ ' who is my agent',
                        'for ' +ent_working_year.get()+ ' years.',
                        '',
                        'I can assure you that ' +ent_applicant_name.get()+ ' has good work ethics and',
                        'exemplary skills. ' +ent_applicant_name.get()+ ' is a constant top performer in our',
                        'department. ' +ent_applicant_name.get()+ ' has always exceeded scorecard metrics',
                        'and received numerous awards.',
                        '',
                        ent_applicant_name.get()+ ' is a team player and proactively help his/her',
                        'teammates regarding hard technical issues.',
                        '',
                        'I strongly recommend ' +ent_applicant_name.get()+ ' as an excellent and professional',
                        'worker. I am willing to provide more information if needed.',
                        '',
                        '',
                        'Warm regards,'
                        ]        
                end_part=[
                ent_your_name.get(),
                ent_job_position.get(),
                ent_company_name.get(),
                ent_number.get(),
                ent_email.get()
                ]




                pdf = canvas.Canvas(fileName)
                pdf.setTitle(documentTitle)

                pdf.setFont('Times-Bold', 36)
                pdf.drawCentredString(348, 725, title)




                dt = pdf.beginText(480,670)
                dt.setFont("Helvetica-Bold", 12)
                dt.setFillColor(colors.black)
                dt.textLine(ent_date.get())
                pdf.drawText(dt)

                regards=pdf.beginText(122,615)
                regards.setFont("Helvetica-Bold", 15)
                regards.setFillColor(colors.black)
                regards.textLine(text='To '+ent_to_whom.get())
                pdf.drawText(regards)


                text = pdf.beginText(122, 570)
                text.setFont("Times-Roman", 15)
                text.setFillColor(colors.black)
                for line in main_part:
                    text.textLine(line)
                pdf.drawText(text)

                text.setFillColor(colors.black)



                txt = pdf.beginText(122, 230)
                txt.setFont("Times-Roman", 15)
                txt.setFillColor(colors.black)
                for lin in end_part:
                    txt.textLine(lin)
                pdf.drawText(txt)

                pdf.setStrokeColor(colors.darkslategrey)
                pdf.setFillColor(colors.darkslategray)
                pdf.rect(0,0,70,1000,stroke=1,fill=1)

                pdf.setStrokeColor(colors.khaki)
                pdf.setFillColor(colors.khaki)
                pdf.rect(71,0,35,1200,stroke=1,fill=1)

                pdf.setStrokeColor(colors.gray)
                pdf.setFillColor(colors.gray)
                pdf.rect(122,260,215,7,stroke=1,fill=1)


                pdf.save()
                
                ent_applicant_name.delete(0,END)
                ent_company_name.delete(0,END)
                ent_date.delete(0,END)
                ent_email.delete(0,END)
                ent_filename.delete(0,END)
                ent_job_position.delete(0,END)
                ent_email.delete(0,END)
                ent_to_whom.delete(0,END)
                ent_Gender.delete(0,END)
                ent_number.delete(0,END)
                ent_working_year.delete(0,END)
                ent_your_name.delete(0,END)



            root=Tk( )
            root.title("Rcommendation frame")#name of window
            f=Frame(root)



            lbl_filename=Label(root,text="Name to be given to file:")
            lbl_filename.place(x=10,y=10)
            ent_filename=Entry(root,bd=4)
            ent_filename.place(x=150,y=10)



            lbl_date=Label(root,text="Enter date(MM DD,YY):")
            lbl_date.place(x=10,y=50)
            ent_date=Entry(root,bd=4)
            ent_date.place(x=150,y=50)

            lbl_to_whom=Label(root,text="To whom you want to \nsend the letter:")
            lbl_to_whom.place(x=10,y=90)
            ent_to_whom=Entry(root,bd=4)
            ent_to_whom.place(x=150,y=90)

            lbl_your_name=Label(root,text="Enter your name:")
            lbl_your_name.place(x=10,y=130)
            ent_your_name=Entry(root,bd=4)
            ent_your_name.place(x=150,y=130)

            lbl_number=Label(root,text="Enter your mobile number:")
            lbl_number.place(x=10,y=170)
            ent_number=Entry(root,bd=4)
            ent_number.place(x=150,y=170)

            lbl_email=Label(root,text="Enter your email-id:")
            lbl_email.place(x=10,y=210)
            ent_email=Entry(root,bd=4)
            ent_email.place(x=150,y=210)


            lbl_job_position=Label(root,text="Enter your job position:")
            lbl_job_position.place(x=425,y=10)
            ent_job_position=Entry(root,bd=4)
            ent_job_position.place(x=575,y=10)

            lbl_company_name=Label(root,text="Enter your company name:")
            lbl_company_name.place(x=425,y=50)
            ent_company_name=Entry(root,bd=4)
            ent_company_name.place(x=575,y=50)

            lbl_applicant_name=Label(root,text="Enter applicants name:")
            lbl_applicant_name.place(x=425,y=90)
            ent_applicant_name=Entry(root,bd=4)
            ent_applicant_name.place(x=575,y=90)

            lbl_working_year=Label(root,text="Enter number of years for\n which you and applicant\n are working together:")
            lbl_working_year.place(x=425,y=170)
            ent_working_year=Entry(root,bd=4)
            ent_working_year.place(x=575,y=185)


            lbl_Gender=Label(root,text="Enter applicants Gender:")
            lbl_Gender.place(x=425,y=130)
            ent_Gender=Entry(root,bd=4)
            ent_Gender.place(x=575,y=130)




            btn=Button(root,text="download",command=reader_recommendation)
            btn.place(x=350,y=270)
            lbl=Label(root,text="click download")
            lbl.place(x=340,y=300)

            root.geometry('800x350+300+155')
            root.mainloop( )
        elif letter.get()==3:
            def reader_resignation():
                fileName = ent_filename.get()+".pdf"
                documentTitle="Resignation letter"
                title = 'Resignation letter'

                pdf = canvas.Canvas(fileName)
                pdf.setTitle(documentTitle)

                main1=[
                'Your organization has helped me shaping well but I have decided to get ',
                'ahead in my life and progress. I would like to inform you about my intent to ',
                'tender my resignation from services with ' +ent_company.get(),
                '',
                '',
                'Last ' +ent_total_years.get()+ ' years with the company has been enriching and value adding to my life.',
                'I truely value the expirience,training and knowledge I gained',
                'over last ' +ent_total_years.get()+ ' years I will treasure this great experience for a life time.',
                'My last date with the company will be ' +ent_last_date.get()+'.',
                'Request you to Please release from my duty and guide me on the ',
                'process of relieving / handover of Job responsibilities.',
                '',
                '',
                'I take this opportunity to thank the Senior Management, Superiors ',
                'and colleagues for the immense support and guidance bestowed on me.',
                'Please let me know how can i help during this transition I wish you',
                'all the best as the company continues to grow.'
                ]



                pdf.setFont('Times-Bold', 36)
                pdf.drawCentredString(300, 760, title)

                for_date= pdf.beginText(70,690)
                for_date.setFont("Helvetica-Bold", 14)
                for_date.setFillColor(colors.black)
                for_date.textLine(ent_date.get())
                pdf.drawText(for_date)

                for_recipant= pdf.beginText(70,660)
                for_recipant.setFont("Helvetica-Bold", 14)
                for_recipant.setFillColor(colors.black)
                for_recipant.textLine(ent_reciptant_name.get())
                pdf.drawText(for_recipant)

                for_position= pdf.beginText(70,645)
                for_position.setFont("Helvetica-Bold", 14)
                for_position.setFillColor(colors.black)
                for_position.textLine(ent_position.get())
                pdf.drawText(for_position)


                for_company= pdf.beginText(70,610)
                for_company.setFont("Helvetica-Bold", 14)
                for_company.setFillColor(colors.black)
                for_company.textLine(ent_company.get())
                pdf.drawText(for_company)

                for_street= pdf.beginText(70,593)
                for_street.setFont("Helvetica-Bold", 14)
                for_street.setFillColor(colors.black)
                for_street.textLine(ent_street.get())
                pdf.drawText(for_street)

                for_pin= pdf.beginText(70,576)
                for_pin.setFont("Helvetica-Bold", 14)
                for_pin.setFillColor(colors.black)
                for_pin.textLine(ent_pincode.get())
                pdf.drawText(for_pin)

                for_greet= pdf.beginText(70,542)
                for_greet.setFont("Helvetica-Bold", 14)
                for_greet.setFillColor(colors.black)
                for_greet.textLine(text="dear " +ent_reciptant_name.get(),)
                pdf.drawText(for_greet)

                for_main = pdf.beginText(70, 520)
                for_main.setFont("Times-Roman", 15)
                for_main.setFillColor(colors.black)
                for line in main1:
                    for_main.textLine(line)
                pdf.drawText(for_main)


                regards= pdf.beginText(70,175)
                regards.setFont("Times-Roman", 15)
                regards.setFillColor(colors.black)
                regards.textLine(text='Thanks&regards')
                pdf.drawText(regards)

                regards_name= pdf.beginText(70,155)
                regards_name.setFont("Times-Roman", 15)
                regards_name.setFillColor(colors.black)
                regards_name.textLine(ent_your_name.get())
                pdf.drawText(regards_name)


                pdf.setStrokeColor(colors.gray)
                pdf.setFillColor(colors.gray)
                pdf.rect(70,135,200,7,stroke=1,fill=1)

                pdf.setStrokeColor(colors.darkslategray)
                pdf.setFillColor(colors.darkslategray)
                pdf.rect(23,10,5,810,stroke=0,fill=1)

                pdf.setStrokeColor(colors.darkslategray)
                pdf.setFillColor(colors.darkslategray)
                pdf.rect(570,10,5,810,stroke=0,fill=1)

                pdf.setStrokeColor(colors.darkslategray)
                pdf.setFillColor(colors.darkslategray)
                pdf.rect(23,815,550,5,stroke=0,fill=1)

                pdf.setStrokeColor(colors.darkslategray)
                pdf.setFillColor(colors.darkslategray)
                pdf.rect(23,10,550,5,stroke=0,fill=1)


                pdf.setStrokeColor(colors.khaki)
                pdf.setFillColor(colors.khaki)
                pdf.rect(18,5,563,820,stroke=1,fill=0)

                pdf.setStrokeColor(colors.khaki)
                pdf.setFillColor(colors.khaki)
                pdf.rect(35,20,528,790,stroke=1,fill=0)




                pdf.save()

                ent_last_date.delete(0,END)
                ent_company.delete(0,END)
                ent_date.delete(0,END)
                ent_filename.delete(0,END)
                ent_pincode.delete(0,END)
                ent_position.delete(0,END)
                ent_reciptant_name.delete(0,END)
                ent_street.delete(0,END)
                ent_total_years.delete(0,END)
                ent_your_name.delete(0,END)







            root=Tk( )
            root.title("Resignation frame")#name of window
            f=Frame(root)







            lbl_filename=Label(root,text="Name to be given to file:")
            lbl_filename.place(x=10,y=10)
            ent_filename=Entry(root,bd=4)
            ent_filename.place(x=150,y=10)


            lbl_date=Label(root,text="DATE(month date,year):")
            lbl_date.place(x=10,y=50)
            ent_date=Entry(root,bd=4)
            ent_date.place(x=150,y=50)


            lbl_reciptant_name=Label(root,text="Recipant's name:-")
            lbl_reciptant_name.place(x=10,y=90)
            ent_reciptant_name=Entry(root,bd=4)
            ent_reciptant_name.place(x=150,y=90)

            lbl_your_name=Label(root,text="Your name:")
            lbl_your_name.place(x=10,y=130)
            ent_your_name=Entry(root,bd=4)
            ent_your_name.place(x=150,y=130)


            lbl_position=Label(root,text="Position of Recipant:-")
            lbl_position.place(x=10,y=170)
            ent_position=Entry(root,bd=4)
            ent_position.place(x=150,y=170)

            lbl_company=Label(root,text="Company Name")
            lbl_company.place(x=425,y=10)
            ent_company=Entry(root,bd=4)
            ent_company.place(x=575,y=10)

            lbl_street=Label(root,text="Company's street number,\nadderess:")
            lbl_street.place(x=425,y=50)
            ent_street=Entry(root,bd=4)
            ent_street.place(x=575,y=50)

            lbl_pincode=Label(root,text="Pincode:")
            lbl_pincode.place(x=425,y=90)
            ent_pincode=Entry(root,bd=4)
            ent_pincode.place(x=575,y=90)

            lbl_total_years=Label(root,text="No. of years you worked \nwith this company")
            lbl_total_years.place(x=425,y=130)
            ent_total_years=Entry(root,bd=4)
            ent_total_years.place(x=575,y=130)


            last_date="6th Sept, 2017" #input("enter your last date with company:")
            lbl_last_date=Label(root,text="Your last date with \ncompany (DD Month,YY):")
            lbl_last_date.place(x=425,y=170)
            ent_last_date=Entry(root,bd=4)
            ent_last_date.place(x=575,y=170)


            btn=Button(root,text="download",command=reader_resignation)
            btn.place(x=350,y=270)
            lbl=Label(root,text="click download")
            lbl.place(x=340,y=295)


            root.geometry('800x350+300+180')
            root.mainloop( )  
        elif letter.get()==4:
            def reader_cover():

                fileName = ent_filename.get()+".pdf"
                documentTitle="Cover letter"

                main_part=[
                'My name is ' +ent_name.get()+ ',I am thrilled to be applying for the ' +ent_job.get()+ ' role',
                'in your company. After reviewing your job description, it is clear that you are looking',
                'for an enthusiastic applicant that can be relied upon to fully engage with the role',
                'and develop professionally in a self-motivated manner. Given these requirements,',
                'Having an ability of managing multiple stakeholders at the same time, ',
                'I believe I meet all the essential criteria for the position.',
                '',
                '',
                'Considering my track record in overcoming complex business challenges and making',
                'high stakes decisions using experience-backed judgment, strong work ethics ',
                'and integrity, I see significant opportunities in aligning myself with your organization.',
                '',
                '',
                'Interested in building a career which is intellectually challenging and personally ',
                'rewarding being a valuable team member, contributing quality ideas and work for an  ',
                'organization where there is an ample scope for individual as well as organization growth.',
                '',
                '',
                'I appreciate your efforts in taking the time to review my credentials and experience.',
                'Looking forward to a positive response.',
                '',
                '',
                '',
                'Thanking you,',
                '',
                'Sincerely,',
                '',
                ent_name.get()
                ]



                pdf = canvas.Canvas(fileName)
                pdf.setTitle(documentTitle)


                pdf.setFont('Helvetica-Bold', 28)
                pdf.drawCentredString(300, 760, ent_name.get())

                pdf.setFont('Times-Roman', 13)
                pdf.drawCentredString(300, 740, ent_address.get())

                pdf.setFont('Times-Roman', 13)
                pdf.drawCentredString(300, 720, ent_phone.get())

                pdf.setFont('Times-Roman', 13)
                pdf.setFillColor(colors.blue)
                pdf.drawCentredString(300, 700, ent_email.get())


                pdf.setFillColor(colors.gray)
                pdf.rect(100,690,430,4,stroke=0,fill=1)

                dt = pdf.beginText(460,670)
                dt.setFont("Helvetica-Bold", 12)
                dt.setFillColor(colors.black)
                dt.textLine(ent_date.get())
                pdf.drawText(dt)

                greet = pdf.beginText(80,610)
                greet.setFont("Times-Roman", 13)
                greet.setFillColor(colors.black)
                greet.textLine(text="Dear Sir/ Madam,")
                pdf.drawText(greet)

                text = pdf.beginText(80, 580)
                text.setFont("Times-Roman", 13)
                text.setFillColor(colors.black)
                for line in main_part:
                    text.textLine(line)
                pdf.drawText(text)



                pdf.save()

                ent_address.delete(0,END)
                ent_email.delete(0,END)
                ent_date.delete(0,END)
                ent_filename.delete(0,END)
                ent_job.delete(0,END)
                ent_name.delete(0,END)
                ent_phone.delete(0,END)


            root=Tk( )
            root.title("Cover Letter frame")
            f=Frame(root)

            lbl_filename=Label(root,text="Name to be given to file:")
            lbl_filename.place(x=10,y=10)
            ent_filename=Entry(root,bd=4)
            ent_filename.place(x=150,y=10)

            lbl_name=Label(root,text="Your name:")
            lbl_name.place(x=10,y=50)
            ent_name=Entry(root,bd=4)
            ent_name.place(x=150,y=50)

            lbl_address=Label(root,text="Your address")
            lbl_address.place(x=10,y=90)
            ent_address=Entry(root,bd=4)
            ent_address.place(x=150,y=90)

            lbl_phone=Label(root,text="Your Phone number ")
            lbl_phone.place(x=425,y=10)
            ent_phone=Entry(root,bd=4)
            ent_phone.place(x=575,y=10)

            lbl_email=Label(root,text="Your email-id ")
            lbl_email.place(x=425,y=50)
            ent_email=Entry(root,bd=4)
            ent_email.place(x=575,y=50)


            lbl_date=Label(root,text="DATE(DD Month,YY):")
            lbl_date.place(x=425,y=90)
            ent_date=Entry(root,bd=4)
            ent_date.place(x=575,y=90)

            lbl_job=Label(root,text="JOB position")
            lbl_job.place(x=425,y=130)
            ent_job=Entry(root,bd=4)
            ent_job.place(x=575,y=130)

            btn=Button(root,text="download",command=reader_cover)
            btn.place(x=330,y=200)
            lbl=Label(root,text="click download")
            lbl.place(x=325,y=220)


            root.geometry('800x250+350+225')
            root.mainloop( )        
        elif letter.get()==5:
            def reader_leave():
                fileName = ent_filename.get()+".pdf"
                documentTitle="Leave letter"
                tittle="Annual Leave Letter"
                main_part=[
                    'To,',
                    ent_boss.get(),
                    ent_address.get(),
                    ent_code.get(),
                    '',
                    '',
                    'Subject: '+ent_subject.get(),
                    '',
                    '',
                    'dear '+ent_boss.get()+',',
                    '',
                    'I would like to officially inform you regarding my absence from office for ' +ent_weeks.get()+ ' weeks.',
                    'I would be going to ' +ent_location.get()+ ' in order to avail my annual leave allowance',
                    'provided by the company. I have sufficient hours accumulated to cover this time off.',
                    '',
                    'I am moving on ' +ent_date1.get()+ ' and returning on '+ent_date2.get()+'.',
                    '',
                    'In the meantime, I have discussed my request with my colleagues and my tasks will be ',
                    'taken care by ' +ent_collegue.get()+ ' in my absence who will be submitting the weekly ',
                    'reports to you on regular intervals. I am available on my phone ' +ent_phone.get()+ ' and ',
                    'email:' +ent_email.get(), ' in case you need to contact me for important questions.',
                    '',
                    'I hope you will consider my request and grant me leaves for the above mentioned dates.',
                    '',
                    'If you have any questions, please feel free to contact me directly.',
                    'Looking forward to your approval.',
                    '',
                    '',
                    'Yours sincerely,',
                    '',
                    ent_name.get()
                ]





                pdf = canvas.Canvas(fileName)
                pdf.setTitle(documentTitle)

                pdf.setFont('Helvetica-Bold', 28)
                pdf.drawCentredString(300, 760, tittle)

                text = pdf.beginText(80, 700)
                text.setFont("Times-Roman", 13)
                text.setFillColor(colors.black)
                for line in main_part:
                    text.textLine(line)
                pdf.drawText(text)

                pdf.save()
                
                ent_filename.delete(0,END)
                ent_boss.delete(0,END)
                ent_address.delete(0,END)
                ent_code.delete(0,END)
                ent_collegue.delete(0,END)
                ent_date1.delete(0,END)
                ent_date2.delete(0,END)
                ent_email.delete(0,END)
                ent_location.delete(0,END)
                ent_name.delete(0,END)
                ent_phone.delete(0,END)
                ent_subject.delete(0,END)
                ent_weeks.delete(0,END)
            



            root = Tk( )#creating instance of window
            root.title("Leave letter")#name of window
            f=Frame(root)




            lbl_filename=Label(root,text="Name to be given to file")
            lbl_filename.place(x=10,y=10)
            ent_filename=Entry(root,bd=4)
            ent_filename.place(x=150,y=10)


            lbl_boss=Label(root,text="boss name")
            lbl_boss.place(x=10,y=50)
            ent_boss=Entry(root,bd=4)
            ent_boss.place(x=150,y=50)

            lbl_address=Label(root,text="Address")
            lbl_address.place(x=10,y=90)
            ent_address=Entry(root,bd=4)
            ent_address.place(x=150,y=90)

            lbl_code=Label(root,text="district code")
            lbl_code.place(x=10,y=130)
            ent_code=Entry(root,bd=4)
            ent_code.place(x=150,y=130)

            lbl_subject=Label(root,text="Subject for leave")
            lbl_subject.place(x=10,y=170)
            ent_subject=Entry(root,bd=4)
            ent_subject.place(x=150,y=170)

            lbl_location=Label(root,text="where you are going")
            lbl_location.place(x=10,y=210)
            ent_location=Entry(root,bd=4)
            ent_location.place(x=150,y=210)

            lbl_date1=Label(root,text="date when you are leaving")
            lbl_date1.place(x=10,y=250)
            ent_date1=Entry(root,bd=4)
            ent_date1.place(x=150,y=250)

            lbl_date2=Label(root,text="date when you are\nreturning")
            lbl_date2.place(x=425,y=10)
            ent_date2=Entry(root,bd=4)
            ent_date2.place(x=575,y=18)

            lbl_collegue=Label(root,text="name of collegue who\n will handle your work")
            lbl_collegue.place(x=425,y=65)
            ent_collegue=Entry(root,bd=4)
            ent_collegue.place(x=575,y=73)

            lbl_phone=Label(root,text="Your phone number")
            lbl_phone.place(x=425,y=110)
            ent_phone=Entry(root,bd=4)
            ent_phone.place(x=575,y=110)

            lbl_email=Label(root,text="Your Email-id")
            lbl_email.place(x=425,y=150)
            ent_email=Entry(root,bd=4)
            ent_email.place(x=575,y=150)

            lbl_weeks=Label(root,text="total weeks you are\n going for")
            lbl_weeks.place(x=425,y=190)
            ent_weeks=Entry(root,bd=4)
            ent_weeks.place(x=575,y=195)

            lbl_name=Label(root,text="Your name")
            lbl_name.place(x=425,y=250)
            ent_name=Entry(root,bd=4)
            ent_name.place(x=575,y=250)

            btn=Button(root,text="download",command=reader_leave)
            btn.place(x=330,y=350)
            lbl=Label(root,text="click download")
            lbl.place(x=325,y=370)

            root.geometry('750x400+350+155')
            root.mainloop( )
       




top = Tk( )
top.title("Select Form")

lbl_head=Label(top,text='LETTER GENERATOR')
lbl_head.place(x=100,y=10)

Font_tuple = ("Comic Sans MS", 20, "bold")
lbl_head.configure(font = Font_tuple)

letter= IntVar()

rb_resume=Radiobutton(top,text="Resume",variable=letter,value=1)
rb_resume.place(x=180,y=60)
Font_tuple = ("Cambria", 15)
rb_resume.configure(font = Font_tuple)

rb_reccommendation=Radiobutton(top,text="Recomendation",variable=letter,value=2)
rb_reccommendation.place(x=180,y=90)
Font_tuple = ("Cambria", 15)
rb_reccommendation.configure(font = Font_tuple)

rb_resignation=Radiobutton(top,text="Resignation",variable=letter,value=3)
rb_resignation.place(x=180,y=120)
Font_tuple = ("Cambria", 15)
rb_resignation.configure(font = Font_tuple)

rb_cover=Radiobutton(top,text="Cover",variable=letter,value=4)
rb_cover.place(x=180,y=150)
Font_tuple = ("Cambria", 15)
rb_cover.configure(font = Font_tuple)

rb_leave=Radiobutton(top,text="Leave",variable=letter,value=5)
rb_leave.place(x=180,y=180)
Font_tuple = ("Cambria", 15)
rb_leave.configure(font = Font_tuple)

btn_read=Button(top,text="Next",command=reader)
btn_read.place(x=210,y=220)

lbl_=Label(top,text='After selecting click next')
lbl_.place(x=170,y=250)

top.geometry('500x300+500+200')#width height x and y position to display
top.mainloop( )

